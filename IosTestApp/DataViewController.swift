//
//  DataViewController.swift
//  IosTestApp
//
//  Created by Łukasz Kamiński on 01.07.2016.
//  Copyright © 2016 Łukasz Kamiński. All rights reserved.
//

import UIKit

class DataViewController: UIViewController {

    @IBOutlet weak var dataLabel: UILabel!
    var dataObject: String = ""
    var dataDesc: String = ""
    @IBOutlet weak var desc: UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.dataLabel!.text = dataObject
        self.desc!.text = dataDesc
    }


}

